package com.simat_persona_estado_inscripcion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.simat_persona_estado_inscripcion.entity.PersonasEstadoInscripcionEntity;
import com.simat_persona_estado_inscripcion.service.PersonasEstadoInscripcionService;

@RestController
@RequestMapping("/persona")
public class PersonasEstadoInscripcionController {

	@Autowired
	PersonasEstadoInscripcionService personaService;

	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public ResponseEntity<String> status() {
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}

	@RequestMapping(value = "/getPersona/{idPersona}/estado/inscripcion", method = RequestMethod.GET)
	public ResponseEntity<PersonasEstadoInscripcionEntity> getPersonaEstadoInscripcion(
			@PathVariable("idPersona") String idPersona) {
		try {
			PersonasEstadoInscripcionEntity personaEntity = personaService.getPersonaEstadoInscripcion(
					idPersona);
			return new ResponseEntity<>(personaEntity, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
