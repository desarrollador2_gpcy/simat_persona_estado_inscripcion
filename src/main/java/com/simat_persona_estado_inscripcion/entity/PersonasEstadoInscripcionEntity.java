package com.simat_persona_estado_inscripcion.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONAS")
public class PersonasEstadoInscripcionEntity {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "ANO")
	private Integer ano;

	@Column(name = "NUM_FORMULARIO")
	private Integer numFormulario;	
	
	@Column(name = "FEC_DIGITACION")
	private Date fecDigitacion;	
	
	@Column(name = "FEC_RECEPCION")
	private Date fecRecepcion;	

	@Column(name = "COMENTARIO")
	private String comentario;
	
	@Column(name = "JER_ID")
	private Integer jerId;
	
	@Column(name = "CTE_ID_ESTADO_INS")
	private Integer cteIdEstadoIns;
	
	@Column(name = "PER_ID_ALUMNO")
	private Integer perIdAlumno;

	@Column(name = "CTE_ID_GRADO")
	private Integer cteIdAlumno;

	@Column(name = "CANCELADA")
	private String cancelada;
	
	@Column(name = "VIENE_SECTOR_PRIVADO")
	private String vieneSectorPrivado;
	
	@Column(name = "VIENE_OTRO_MUNICIPIO")
	private String vieneOtroMunicipio;
	
	@Column(name = "INSTITUCION_BIENESTAR")
	private String institucionBienestar;
	
	@Column(name = "CTE_ID_SITUACION_ANO_ANTERIOR")
	private Integer cteIdSituacionAnoAnterior;

	@Column(name = "AUDIT_USER")	
	private String auditUser;

	@Column(name = "AUDIT_PAGE")
	private String auditPage;
	
	@Column(name = "AUDIT_DATE")
	private Date auditDate;
	
	@Column(name = "CAM_CALENDARIO")
	private String camCalenadrio;
	
	@Column(name = "NOMBRE_ACUDIENTE")
	private String nombreAcudiente;

	@Column(name = "EMAIL")
	private String email;
		
	@Column(name = "TEL_ACUDIENTE")
	private String telAcudiente;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getNumFormulario() {
		return numFormulario;
	}

	public void setNumFormulario(Integer numFormulario) {
		this.numFormulario = numFormulario;
	}

	public Date getFecDigitacion() {
		return fecDigitacion;
	}

	public void setFecDigitacion(Date fecDigitacion) {
		this.fecDigitacion = fecDigitacion;
	}

	public Date getFecRecepcion() {
		return fecRecepcion;
	}

	public void setFecRecepcion(Date fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Integer getJerId() {
		return jerId;
	}

	public void setJerId(Integer jerId) {
		this.jerId = jerId;
	}

	public Integer getCteIdEstadoIns() {
		return cteIdEstadoIns;
	}

	public void setCteIdEstadoIns(Integer cteIdEstadoIns) {
		this.cteIdEstadoIns = cteIdEstadoIns;
	}

	public Integer getPerIdAlumno() {
		return perIdAlumno;
	}

	public void setPerIdAlumno(Integer perIdAlumno) {
		this.perIdAlumno = perIdAlumno;
	}

	public Integer getCteIdAlumno() {
		return cteIdAlumno;
	}

	public void setCteIdAlumno(Integer cteIdAlumno) {
		this.cteIdAlumno = cteIdAlumno;
	}

	public String getCancelada() {
		return cancelada;
	}

	public void setCancelada(String cancelada) {
		this.cancelada = cancelada;
	}

	public String getVieneSectorPrivado() {
		return vieneSectorPrivado;
	}

	public void setVieneSectorPrivado(String vieneSectorPrivado) {
		this.vieneSectorPrivado = vieneSectorPrivado;
	}

	public String getVieneOtroMunicipio() {
		return vieneOtroMunicipio;
	}

	public void setVieneOtroMunicipio(String vieneOtroMunicipio) {
		this.vieneOtroMunicipio = vieneOtroMunicipio;
	}

	public String getInstitucionBienestar() {
		return institucionBienestar;
	}

	public void setInstitucionBienestar(String institucionBienestar) {
		this.institucionBienestar = institucionBienestar;
	}

	public Integer getCteIdSituacionAnoAnterior() {
		return cteIdSituacionAnoAnterior;
	}

	public void setCteIdSituacionAnoAnterior(Integer cteIdSituacionAnoAnterior) {
		this.cteIdSituacionAnoAnterior = cteIdSituacionAnoAnterior;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	public String getAuditPage() {
		return auditPage;
	}

	public void setAuditPage(String auditPage) {
		this.auditPage = auditPage;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getCamCalenadrio() {
		return camCalenadrio;
	}

	public void setCamCalenadrio(String camCalenadrio) {
		this.camCalenadrio = camCalenadrio;
	}

	public String getNombreAcudiente() {
		return nombreAcudiente;
	}

	public void setNombreAcudiente(String nombreAcudiente) {
		this.nombreAcudiente = nombreAcudiente;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelAcudiente() {
		return telAcudiente;
	}

	public void setTelAcudiente(String telAcudiente) {
		this.telAcudiente = telAcudiente;
	}
	
	
}
