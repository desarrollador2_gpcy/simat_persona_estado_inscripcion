package com.simat_persona_estado_inscripcion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simat_persona_estado_inscripcion.entity.PersonasEstadoInscripcionEntity;
import com.simat_persona_estado_inscripcion.repository.PersonasEstadoInscripcionRepository;
import com.simat_persona_estado_inscripcion.service.PersonasEstadoInscripcionService;

@Service
public class PersonasEstadoInscripcionServiceImpl implements PersonasEstadoInscripcionService {

	@Autowired
	private PersonasEstadoInscripcionRepository personaRepository;

	public PersonasEstadoInscripcionEntity getPersonaEstadoInscripcion( String idPersona) {
		return personaRepository.getPersonaEstadoInscripcion(idPersona);
	}

}
