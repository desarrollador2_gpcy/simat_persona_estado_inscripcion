package com.simat_persona_estado_inscripcion.service;

import com.simat_persona_estado_inscripcion.entity.PersonasEstadoInscripcionEntity;

public interface PersonasEstadoInscripcionService {

	public PersonasEstadoInscripcionEntity getPersonaEstadoInscripcion(String idPersona);

}
