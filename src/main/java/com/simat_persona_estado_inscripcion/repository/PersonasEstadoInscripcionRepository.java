package com.simat_persona_estado_inscripcion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.simat_persona_estado_inscripcion.entity.PersonasEstadoInscripcionEntity;

@Repository
public interface PersonasEstadoInscripcionRepository extends JpaRepository<PersonasEstadoInscripcionEntity, Integer> {
	
	@Query(value = "SELECT * FROM INSCRIPCIONES "
			+ "WHERE PER_ID_ALUMNO=:idPersona" , nativeQuery = true)
	PersonasEstadoInscripcionEntity getPersonaEstadoInscripcion(
			@Param("idPersona") String idPersona);
	
}
